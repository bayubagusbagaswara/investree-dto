package com.investree.demo.dto;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Data
public class MahasiswaDTO {
    public MahasiswaDTO(Long id, String nama, String nim, String alamat) {
        this.id = id;
        this.nama = nama;
        this.nim = nim;
        this.alamat = alamat;
    }
    private Long id;

    private String nama;

    private String nim;

    private String alamat;
}

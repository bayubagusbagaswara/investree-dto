package com.investree.demo.view;


import com.investree.demo.dto.MahasiswaDTO;
import com.investree.demo.model.Mahasiswa;

import java.util.Map;

public interface MahasiswaInterface {
    public Map insert(Mahasiswa obj);
    public Map update(Mahasiswa obj);
    public Map delete(Long obj);
    public MahasiswaDTO MahasiswaWithDTO(Mahasiswa obj);
}
